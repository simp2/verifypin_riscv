import os
import sys
from pin_app import PINApp, INVALID, VALID, LOCKED, UNDER_ATTACK
from tqdm import tqdm
from termcolor import colored, cprint

if __name__ == '__main__':

    script_path = os.path.dirname(os.path.abspath(__file__))
    elf_path = os.path.abspath(script_path + "/../application/build/pin.elf")
    core_path = os.path.abspath(script_path + "/../Comet/build/bin/comet.sim")

    if len(sys.argv) > 1:
        # sys.argv[1]
        cprint("No argument for this script.", "red")
        sys.exit()

    
    app = PINApp(elf_path, core_path, 3, verbose=False)

    for candidate in tqdm(range(10000)):
        (r1, _, _) = app.run(str(candidate))

        if r1 ==  VALID:
            cprint("Secret PIN is %i" %candidate, "green")
            sys.exit()
        elif r1 == LOCKED or r1 == UNDER_ATTACK:
            cprint("Device locked. End of brute force attack.", "red")
            sys.exit()
