import os
import sys
from pin_app import PINApp, INVALID, VALID, LOCKED
from termcolor import colored, cprint

if __name__ == '__main__':

    script_path = os.path.dirname(os.path.abspath(__file__))
    elf_path = os.path.abspath(script_path + "/../application/build/pin.elf")
    core_path = os.path.abspath(script_path + "/../Comet/build/bin/comet.sim")

    if len(sys.argv) > 1:
        # sys.argv[1]
        cprint("No argument for this script.", "red")
        sys.exit()

    
    app = PINApp(elf_path, core_path, 3, verbose=False)
    (r1, _, _) = app.run("0000")
    assert r1 == INVALID, "Must return INVALID upon an incorrect candidate (here 0000)." # Invalid

    (r2, c2, _) = app.run("3141")
    assert r2 == VALID, "Must return VALID upon a correct candidate (here 3141)." # Valid

    for _ in range(c2-1):
        app.run("0000")

    (r3, c3, _) = app.run("0000")
    assert r3 == LOCKED, "Must be LOCKED after %i incorrect candidates." %c2 # Locked

    cprint("Your implementation is working!", "green")

    # for add in app.verifypin_range:
    #     print("Fault injection at 0x%x: " %add, end="")
    #     res = app.run_fault("0000", str(add), "1")
    #     if res != None:
    #         (r, c, _) = res
    
