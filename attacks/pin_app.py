from elftools.elf.elffile import ELFFile
from elftools.elf.sections import NoteSection, SymbolTableSection

from termcolor import colored, cprint
import sys
import os
import subprocess
import re

VALID = 0xAA
INVALID = 0xFF
LOCKED = 0x55
UNDER_ATTACK = 0x77

def print_result_code(result_code, tries):
    if result_code == VALID:
        cprint("Valid PIN.", "green")
    elif result_code == INVALID:
        cprint("PIN invalid. %i tries left." % tries, "yellow")
    elif result_code == LOCKED:
        cprint("Device is locked.", "red")
    elif result_code == UNDER_ATTACK:
        cprint("Device is under attack. %i tries left." % tries, "blue")
    else:
        print("Unknown result: %i. %i tries left" % (result_code, tries))

# we read the elf file to automatically extract the address and size of the given symbol
def extract_symbol_range(symbol_name, elf_path):
    e = ELFFile(open(elf_path, 'rb'))
    symbol_tables = [s for s in e.iter_sections() if isinstance(s, SymbolTableSection)]
    for section in symbol_tables:
        for symbol in section.iter_symbols():
            if symbol.name == symbol_name:
                return range(symbol['st_value'], symbol['st_size'] + symbol['st_value'])

def extract_symbol_address(symbol_name, elf_path):
    return extract_symbol_range(symbol_name, elf_path).start

class PINApp:
    def __init__(self, elf_path, core_path, tries, verbose=False):

        # test for elf existence
        if not os.path.exists(elf_path):
            cprint("Your application must be built with ", "red", end="")
            print("./scripts/build-app.sh")
            sys.exit()
        else:
            cprint("Application pin.elf found.", "green")

        # test for core existence
        if not os.path.exists(core_path):
            cprint("Your core must be built with ", "red", end="")
            print("./scripts/build-core.sh")
            sys.exit()
        else:
            cprint("Comet core found.", "green")

        self.verbose = verbose
        self.elf_path = elf_path
        self.core_path = core_path
        self.counter = tries
        self.counter_address = extract_symbol_address("tries_counter", self.elf_path)
        self.verifypin_range = extract_symbol_range("verify_pin", self.elf_path)
        self.verifypin_range = range(self.verifypin_range.start, self.verifypin_range.stop, 4) # No compressed instructions

        self.comparearrays_range = extract_symbol_range("compare_arrays", self.elf_path)
        self.comparearrays_range = range(self.comparearrays_range.start, self.comparearrays_range.stop, 4) # No compressed instructions
        self.pattern = r"Enter your PIN \(4 digits\): (\w\w) (\d+)\nCore cycle: (\d+)"

    def run(self, candidate:str, tries = None):

        if tries == None:
            tries = self.counter
        command = [self.core_path, "-f", self.elf_path, "-M", "-A", str(self.counter_address), "-V", str(tries)]
        process = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        stdout_data, stderr_data = process.communicate(input=candidate)

        result_match = re.search(self.pattern, stdout_data)

        if result_match:
            self.result_code = int(result_match.group(1).lower(), 16)  
            self.counter = int(result_match.group(2))
            self.cycles = int(result_match.group(3))
            
            if self.verbose:
                print_result_code(self.result_code, self.counter)

            return (self.result_code, self.counter, self.cycles)
        else:
            cprint("***CRASH***", "red")
            return (0, 0, 0)

    def run_fault(self, candidate:str, pc_fault, it_fault = 1, tries = None):
        if tries == None:
            tries = self.counter
        command = [self.core_path, "-f", self.elf_path, "-T", str(pc_fault), "-C", str(it_fault), "-M", "-A", str(self.counter_address), "-V", str(tries)]
        process = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        stdout_data, stderr_data = process.communicate(input=candidate)

        result_match = re.search(self.pattern, stdout_data)

        if result_match:
            self.result_code = int(result_match.group(1).lower(), 16) 
            self.counter = int(result_match.group(2))
            self.cycles = int(result_match.group(3))
            
            if self.verbose:
                print_result_code(self.result_code, self.counter)

            return (self.result_code, self.counter, self.cycles)
        else:
            cprint("***CRASH***", "red")
            return (0, 0, 0)