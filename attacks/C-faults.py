import os
import sys
from pin_app import PINApp, INVALID, VALID, LOCKED, UNDER_ATTACK
from tqdm import tqdm
from termcolor import colored, cprint

if __name__ == '__main__':

    script_path = os.path.dirname(os.path.abspath(__file__))
    elf_path = os.path.abspath(script_path + "/../application/build/pin.elf")
    core_path = os.path.abspath(script_path + "/../Comet/build/bin/comet.sim")

    if len(sys.argv) > 1:
        # sys.argv[1]
        cprint("No argument for this script.", "red")
        sys.exit()

    
    app = PINApp(elf_path, core_path, 3, verbose=True)

    # Compare arrays
    for add in app.comparearrays_range:
        print("Fault injection at 0x%x: " %add, end="")
        res = app.run_fault("0000", add, 1, 3)
        if res != None:
            (r, c, _) = res
    
    # Verify PIN
    for add in app.verifypin_range:
        print("Fault injection at 0x%x: " %add, end="")
        res = app.run_fault("0000", add, 1, 3)
        if res != None:
            (r, c, _) = res