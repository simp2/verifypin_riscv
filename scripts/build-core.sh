#!/usr/bin/env nix-shell
#!nix-shell ../env/shell.nix -i bash --pure

eval "$commands"

mkdir -p "$CORE_DIR"
cd "$CORE_DIR"
cmake ..
make