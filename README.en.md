# VerifyPIN RISC-V

Running an application with security functions on an embedded system requires facing a new threat: physical attacks.
The attacker has physical access to the system running your application, which offers numerous possibilities: measuring execution times, listening to the electromagnetic environment, or interacting directly with the circuit...

## Your mission

You have been tasked with developing a PIN verification code on a microcontroller. The system receives 4 digits from 0 to 9 and must compare them to those, secret, stored on the chip.

You have been foresighted, and the memory of your chip is TIP TOP SECURE: the attacker cannot read it once the secret code is there. However, they have a copy of your application, without the secret code, which allows them to prepare their attacks.

Your opponent will do everything to recover this secret, will you be able to counter them?

## Setup

Retrieve this practical work on your machine from the git repository:

```bash
git clone --recurse-submodules https://gitlab.inria.fr/simp2/verifypin_riscv.git
cd verifypin_riscv
```

Install the [Nix package manager](https://nixos.org/) for dependencies:

```bash
sh <(curl -L https://nixos.org/nix/install) --daemon
```

To prepare the working environment and trigger the download of dependencies, you can use the script
```bash
./scripts/install.sh
```

This is optional for the proper conduct of the practical work but allows avoiding network congestion on the day.

## Objective

You must implement a PIN code verification in the file [application/src/pin.c](./application/src/pin.c).

You will see that such a secure implementation is not so evident.
We will start with the software implementation of this verification.
The guide for this implementation is in the file [README.software.md](./README.software.md).

### Compilation

Compiling your application is simply done with a make.
For reproducibility reasons, this step is encapsulated in a script:
```bash
./scripts/build-app.sh
```

Add the *clean* argument to delete compilation artifacts.

After this compilation, you will be able to find your decomplied application [application/build/pin.asm](./application/build/pin.asm).

Open this file and observe the compiler's work!

### Building the (simulator of the) core

The sources of the RISC-V core are in the [Comet](./Comet/) folder.
To build the simulator of the core, call the corresponding script.
```bash
./scripts/build-core.sh
```

### Execution

Now it's time to run your application on the simulator.
Use the script
```bash
./scripts/execute-application.sh
```

Enter your candidate PIN and observe the output.

```
Enter your PIN (4 digits): 1234
ff 2
Core cycle: 4799
```

The results are composed of:
 - a code (here ff for INVALID), the codes are defined in [pin.h](./application/inc/pin.h).
 - the number of remaining attempts (here 2).
 - the number of cycles to execute your application (here 4799).

```
#define LOCKED 0x55
#define VALID 0xAA
#define UNDER_ATTACK 0x77
#define INVALID 0xFF
```

### Launching attacks

Attacks, which use the simulator and your application, are available in the [attacks](./attacks/) folder.
To execute them, use the *launch-attack.sh* script by giving the python file of the attack as an argument. For example.
```bash
./scripts/launch-attack.sh attacks/A-bruteforce.py
```

## Cleaning up after the practical work

At the end of the practical work, you can delete the Nix packages using
```bash
nix-collect-garbage
```