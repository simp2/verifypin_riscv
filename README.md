# VerifyPIN RISC-V

Exécuter une application avec des fonctions de sécurité sur un système embarqué demande de faire face à une nouvelle menace: les attaques physiques.
L'attaquant a un accès physique au système faisant tourner votre application, ce qui lui offre de nombreuses possibilités: mesurer des temps d'exécution, écouter l'environnement électromagnétique, ou interagir directement avec le circuit ...


## Votre mission

Vous avez été chargé de développer un code de vérification de code PIN sur un microcontrôleur. Le système reçoit 4 chiffres de 0 à 9 et doit les comparer à ceux, secrets, mémorisés sur la puce.

Vous avez été prévoyant et la mémoire de votre puce est TIP TOP SECURE: l'attaquant ne peut pas la lire une fois que le code secret y est présent. Il possède toutefois une copie de votre application, sans le code secret, ce qui lui permet de préparer ses attaques.

Votre adversaire fera tout pour récupérer ce secret, saurez-vous le contrer ?


## Mise en place

Récupérez ce TP sur votre machine à partir du dépot git:

```bash
git clone --recurse-submodules https://gitlab.inria.fr/simp2/verifypin_riscv.git
cd verifypin_riscv
```

Installer le [gestionnaire de paquet Nix](https://nixos.org/) pour les dépendances:

```bash
sh <(curl -L https://nixos.org/nix/install) --daemon
```

Pour préparer l'environnement de travail et déclencher le téléchargement des dépendances, vous pouvez utiliser le script
```bash
./scripts/install.sh
```

Ceci est optionnel pour la bonne marche du TP, mais permet d'éviter une congestion réseau le jour J.

## Objectif

Vous devez implémenter une vérification de code PIN dans le fichier [application/src/pin.c](./application/src/pin.c).

Vous verrez qu'une telle implémentation sécurisée n'est pas si évidente.
Nous commencerons par l'implémentation logicielle de cette vérification.
Le guide pour cette implémentation est dans le fichier [README.software.md](./README.software.md).

Dans un second temps vous allez modifier la partie matérielle du processeur, les indications concernant cette opération se trouvent dans le fichier [README.hardware.md](./README.hardware.md)

### Compilation

La compilation de votre application se fait simplement à l'aide d'un make.
Pour des raisons de reproducibilité, cette étape est encapsulée dans un script:
```bash
./scripts/build-app.sh
```

Ajouter l'argument *clean* pour supprimer les artefacts de compilation.

Après cette compilation, vous pourrez trouver votre application décompilée [application/build/pin.asm](./application/build/pin.asm).

Ouvrez ce fichier et observez le travail du compilateur !

### Construction du (simulateur du) cœur

Les sources du cœur RISC-V sont dans le dossier [Comet](./Comet/).
Pour construire le simulateur du cœur, appelez le script correspondant.
```bash
./scripts/build-core.sh
```

### Exécution

Il reste maintenant à exécuter votre application sur le simulateur.
Utilisez le script
```bash
./scripts/execute-application.sh
```

Entrez votre PIN candidat et observez la sortie.

```
Enter your PIN (4 digits): 1234
ff 2
Core cycle: 4799
```

Les résultats sont composés:
 - d'un code (ici ff pour INVALID), les codes sont définis dans [pin.h](./application/inc/pin.h). 
 - du nombre d'essais restant (ici 2).
 - du nombre de cycles pour exécuter votre application (ici 4799).

```
#define LOCKED 0x55
#define VALID 0xAA
#define UNDER_ATTACK 0x77
#define INVALID 0xFF
```

### Lancement des attaques

Des attaques, qui utilisent le simulateur et votre application sont disponibles dans le dossier [attacks](./attacks/).
Pour les exécuter, utilisez le script *launch-attack.sh* en donnant en argumant le fichier python de l'attaque. Par exemple.
```bash
./scripts/launch-attack.sh attacks/A-bruteforce.py
```


## Nettoyage après le TP

À la fin du TP, vous pourrez supprimer les paquets Nix à l'aide de 
```bash
nix-collect-garbage
```
