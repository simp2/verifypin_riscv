# Implémentation materielle

## Rappel

Les informations pour compiler et simuler le coeur Comet sont dans le fichier [README.md](./README.md).

## Structure de Comet

Comet est un processeur pipeliné à cinq étages: `fetch`, `decode`, `execute`, `memory` et `writeback`. Le code lié à chacuns de ces étages de pipeline peut être trouvé dans [core.cpp](./Comet/src/core.cpp), dans les fonctions portant le nom de chaque étage.

Chaque fonction prend en entrée le registre de pipeline de l'étage précédent et produit le contenu de son registre de pipeline respectif. Les différents problèmes de timing et de forwarding sont résolus dans la fonction `doCycle()` qui implémente la logique de fonctionnement du processeur à chaque nouveau coup d'horloge.

## Idée d'implémentation

Les attaques par injection de fautes simulées dans ce TP affectent directement le contenu des instructions récupérées par l'étage `fetch` du processeur.

En observant la représentation binaire des instructions RISC-V on s'aperçois que les deux bits de poids faible restent constants peu importe l'instruction (à `0x3`). Ils n'ont donc aucun impact sur le décodage des instructions.

> L'encodage précis des instructions rv32i eput-être trouvé page 104 de [la specification RISC-V](./riscv-spec-v2.2.pdf)

Nous vous proposons d'utiliser ces deux bits pour stocker une information permettant de vérifier l'intégrité des 30 autres bits de l'instruction.

Pour ce faire, vous pouvez modifier les fonctions `instructionProtectionEncode` et `instructionProtectionDecode` dans le fichier [src/protection.cpp](./Comet/src/protection.cpp).

La fonction `instructionProtectionEncode` rajoute les bits d'intégrité puis renvoie l'instruction modifiée tandis que la fonction `instructionProtectionDecode` vérifie si les bits d'intégrité sont corrects. S'ils ne le sont pas, elle doit retourner `false`.

> `instructionProtectionDecode` est appelée après l'étage fetch.

La fonction `instructionProtectionEncode` est appellée dans le simulateur lorsque la mémoire est chargée avant le démarrage du CPU

> Dans un cas réel, l'insertion des bits d'intégrité doit être faite par le compilateur

Il existe de nombreuses façons d'utiliser ces deux bits afin de vérifier l'intégrité de l'instruction:
- Un des bits peut compter le nombre de bits pairs et l'autre le nombre de bits impairs
- Un des bits peut vérifier la parité de la première moitié de l'instruction et l'autre de la seconde
- Ils peuvent représenter les deux derniers bits d'une operation arithmétique sur la partie "utile" de l'instruction
- ...



