
#ifndef PIN_H
#define PIN_H

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#define PIN_SIZE 4

#define LOCKED 0x55
#define VALID 0xAA
#define UNDER_ATTACK 0x77
#define INVALID 0xFF

bool compare_arrays(const uint8_t* a, const uint8_t* b, size_t len);
int verify_pin(const uint8_t* to_verify, size_t len);

#endif