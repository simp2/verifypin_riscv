#include <stdio.h>
#include <stdlib.h>
#include "pin.h"



uint8_t secret[PIN_SIZE] = {3,1,4,1};
uint8_t candidate[PIN_SIZE + 2];
uint8_t tries_counter = 3;

bool compare_arrays(const uint8_t* a, const uint8_t* b, size_t len) {
    return false; // Remplacer par votre propre fonction
}

int verify_pin(const uint8_t* to_verify, size_t len) {
    return INVALID; // Remplacer par votre propre fonction
}

int main(void)
{
  printf("Enter your PIN (4 digits): ");
  scanf("%4s",candidate);
  for(int i = 0; i < PIN_SIZE; i++) {
    candidate[i] -= 0x30; //ASCII to value conversion
  }

  int result = verify_pin(candidate, PIN_SIZE);
  
  printf("%x %d", result, tries_counter);
  return 0;
}
