# Software Implementation

## Reminder

The information for compiling and executing your code is in the [README.md](./README.md) file.

## Your Implementation

Two functions *compare_arrays* and *verify_pin* are defined in the file [./application/src/pin.c](./application/src/pin.c), you must complete these functions without changing their names.

You need to complete the **compare_arrays** and **verify_pin** functions to implement the PIN verification functionality.

The **compare_arrays** function aims to compare two arrays of identical sizes and return *true* if and only if the array values are identical.

The **verify_pin** function reacts to the comparison result: managing the attempt counter and return values.

- If the candidate PIN is equal to the secret, **verify_pin** returns *VALID* and resets the attempt counter to 3.
- Otherwise, if there are attempts left, it returns **INVALID**, and if no attempts are left, it returns **LOCKED**.
- Finally, if an attack is detected, it returns **UNDER_ATTACK**.

After compilation, the assembler and machine code of your implementation can be seen in the file [./application/build/pin.asm](./application/build/pin.asm).

## The Attacks

### Functionality

At this point, you should have a first version of your PIN verification. To check its functionality, launch the script
```bash
./scripts/launch-attack.sh ./attacks/test.py
```
which should end with
```
Your implementation is working!
```

Correct your implementation in case of errors.

### Brute Force

A brute force attack will try all possible candidate PIN codes. If your implementation of the counter is correct, this should not work.

Test the attack:
```bash
./scripts/launch-attack.sh ./attacks/A-bruteforce.py
```

### Constant Time

Is it possible to exploit the

temporal variabilities of the **compare_arrays** function to find the secret more quickly?

The following attack is given 40 tries to succeed (instead of 10,000).

```bash
./scripts/launch-attack.sh ./attacks/B-time.py
```

If the attack works, your implementation of **compare_arrays** is not in constant time! Correct this for the following attack.

### Direct Fault Injection Attack

The attacker is now capable of sending an electromagnetic pulse to your chip. This pulse will corrupt the instruction being executed, which is replaced by a random value.

The following attack characterizes the behavior of your implementation in the face of fault injections:
```bash
./scripts/launch-attack.sh ./attacks/C-faults.py
```

For each instruction of the *verify_pin* and *compare_arrays* functions, we try a fault injection targeting that instruction, for a candidate PIN of 0000. Each time, the card is reset, including the counter which is reset to its initial value of 3.

Here's an example of the result:
```
Fault injection at 0x1023c: ***CRASH***
Fault injection at 0x10240: PIN invalid. 2 tries left.
Fault injection at 0x10244: PIN invalid. 2 tries left.
Fault injection at 0x10248: PIN invalid. 2 tries left.
Fault injection at 0x1024c: PIN invalid. 3 tries left.
```

A fault injection corrupting the instruction at address 0x1023c causes the chip to crash. At instruction 0x1024c, the counter is not decremented!

Let's look at the corresponding instructions in the file [./application/build/pin.asm](./application/build/pin.asm).

```
00010234 <verify_pin>:
   ...
   1023c:    02912223           sw    s1,36(sp)
   10240:    03212023           sw    s2,32(sp)
   10244:    02812423           sw    s0,40(sp)
   10248:    24818913           addi    s2,gp,584 # 32b08 <tries_counter>
   1024c:    00094403           lbu    s0,0(s2)
```

The instruction at address 0x1024c is a `load`, it's the instruction that loads the counter!

**Can you implement a PIN verification where no injection allows validating a wrong candidate PIN?**

### Indirect Fault Injection Attack

That's not all! We saw that a fault injection could corrupt the counter. We can exploit this by combining it with a brute force attack: we simply prevent the counter from decrementing.

Try the corresponding attack:
```bash
./scripts/launch-attack.sh ./attacks/D-faults-indirect.py
```

If you withstand, congratulations! Otherwise, back to work!

### And Now

You have achieved an implementation that resists a few simple attacks. But other attacks are possible

, and notably combining several fault injections during the same execution is not so difficult. Your implementation would probably not withstand such an attacker.

Therefore, you need to modify the chip itself to achieve more significant hardening.