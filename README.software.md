# Implémentation logicielle

## Rappel

Les informations pour compiler et exécuter votre code sont dans le fichier [README.md](./README.md).

## Votre implémentation

Deux fonctions *compare_arrays* et *verify_pin* sont définies dans le fichier [./application/src/pin.c](./application/src/pin.c), vous devez compléter ces fonctions sans en changer les noms.

Vous devez compléter les fonctions **compare_arrays** et **verify_pin** pour implémenter la fonctionnalité de vérification de code PIN.

La fonction **compare_arrays** a pour but de comparer deux tableaux de tailles identiques et de retourner *true* si et seulement si les valeurs des tableaux sont identiques.

La fonction **verify_pin** réagit au résultat de la comparaison : gestion du compteur d'essais et des valeurs de retour.

- Si le PIN candidat est égal au secret, **verify_pin** retourne *VALID* et remet le compteur d'essais à 3.
- Sinon, s'il reste des essais on retourne **INVALID** et s'il n'y a plus d'essais, on retourne **LOCKED**.
- Enfin si une attaque est détectée, on retourne **UNDER_ATTACK**.

Après compilation, le code assembleur et machine de votre implémentation est visible dans le fichier [./application/build/pin.asm](./application/build/pin.asm).

## Les attaques

### Fonctionnalité

À ce point, vous devez avoir une première version de votre vérification de code PIN. Pour vérifier la fonctionnalité de celle-ci, lancez le script
```bash
./scripts/launch-attack.sh ./attacks/test.py
```
qui doit se terminer par
```
Your implementation is working!
```

Corrigez votre implémentation en cas d'erreur.

### Brute force

Une attaque par force brute va essayer tous les codes PIN candidats possibles. Si votre implémentation de compteur est correcte, cela ne devrait pas marcher.

Testez l'attaque :
```bash
./scripts/launch-attack.sh ./attacks/A-bruteforce.py
```

### Temps constant

Est-il possible d'exploiter les

variabilités temporelles de la fonction **compare_arrays** pour retrouver plus rapidement le secret ?

L'attaque suivante se donne 40 essais pour y arriver (au lieu de 10 000).

```bash
./scripts/launch-attack.sh ./attacks/B-time.py
```

Si l'attaque fonctionne, votre implémentation de **compare_arrays** n'est pas en temps constant ! Rectifiez cela pour l'attaque suivante.

### Attaque par injection de fautes directe

L'attaquant est maintenant capable d'envoyer une impulsion électromagnétique sur votre puce. Cette impulsion va corrompre l'instruction en cours d'exécution, qui est remplacée par une valeur aléatoire.

L'attaque suivante caractérise le comportement de votre implémentation face à des injections de fautes :
```bash
./scripts/launch-attack.sh ./attacks/C-faults.py
```

Pour chaque instruction des fonctions *verify_pin* et *compare_arrays*, nous essayons une injection de faute qui cible cette instruction, pour un PIN candidat de 0000. À chaque fois, la carte est réinitialisée, y compris le compteur qui est remis à sa valeur initiale de 3.

Voici un exemple de résultat :
```
Fault injection at 0x1023c: ***CRASH***
Fault injection at 0x10240: PIN invalid. 2 tries left.
Fault injection at 0x10244: PIN invalid. 2 tries left.
Fault injection at 0x10248: PIN invalid. 2 tries left.
Fault injection at 0x1024c: PIN invalid. 3 tries left.
```

Une injection qui corrompt l'instruction à l'adresse 0x1023c entraîne un crash de la puce. À l'instruction 0x1024c, le compteur n'est pas décrémenté !

Regardons les instructions correspondantes dans le fichier [./application/build/pin.asm](./application/build/pin.asm).

```
00010234 <verify_pin>:
   ...
   1023c:    02912223           sw    s1,36(sp)
   10240:    03212023           sw    s2,32(sp)
   10244:    02812423           sw    s0,40(sp)
   10248:    24818913           addi    s2,gp,584 # 32b08 <tries_counter>
   1024c:    00094403           lbu    s0,0(s2)
```

L'instruction à l'adresse 0x1024c est un `load`, c'est l'instruction qui charge le compteur !

**Pouvez-vous implémenter une vérification de code PIN où aucune injection ne permet de valider un mauvais PIN candidat ?**

### Attaque par injection de fautes indirecte

Ce n'est pas tout ! Nous avons vu qu'une injection de faute permettait de corrompre le compteur. Nous pouvons l'exploiter en la combinant avec une attaque par force brute : nous empêcherons simplement la décrémentation du compteur.

Essayez l'attaque correspondante :
```bash
./scripts/launch-attack.sh ./attacks/D-faults-indirect.py
```

Si vous résistez, bravo ! Sinon, au travail !

### Et maintenant

Vous avez réussi une implémentation qui résiste à quelques attaques simples. Mais d'autres attaques sont possibles, et notamment combiner plusieurs injections de faute au cours d'une même exécution n'est pas si difficile. Votre implémentation ne résisterait probablement pas à un tel attaquant.

Il vous faut donc modifier la puce elle-même pour obtenir un durcissement plus significatif.