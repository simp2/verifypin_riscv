with (import <nixpkgs> {});

stdenv.mkDerivation {
    name = "riscv32-inria-elf";
    version = "0.1";

    src = fetchurl {
        url = "https://gitlab.inria.fr/rlasherm/riscv_gcc_nix/-/raw/master/riscv-medany.tar.xz?inline=false";
        hash =  "sha256-s7heg+LuGyHjfS4LpZRzMFicfy5PDhVYEEWvWUR3LOA=";
    };

    nativeBuildInputs = [autoPatchelfHook ];

    phases = [ "unpackPhase" "installPhase" ];

    installPhase = ''
        mkdir -p $out
        cp -r * $out
    '';
  
}

