let
    pkgs = import (builtins.fetchGit {              
        name = "pinned_nix_packages";                                                 
        url = "https://github.com/nixos/nixpkgs/";                       
        ref = "nixos-23.05";                     
        rev = "da5adce0ffaff10f6d0fee72a02a5ed9d01b52fc";                                           
    }) {};

in

with import <nixpkgs> {};

stdenv.mkDerivation rec {
  pname = "konata";
  version = "0.39";

  src = fetchurl {
    url = "https://github.com/shioyadan/Konata/releases/download/v${version}/konata-linux-x64.tar.gz";
    hash = "sha256-4iwyjHOFI/LZ703MYLn0M3/AT+IKpJn1f6L6S/qfN/M=";
  };

  nativeBuildInputs = [
    # pkgs.autoPatchelfHook
  ];

  buildInputs = with pkgs; [
    # electron
    # chromium
    # gtk3
    # alsa-lib
    # nss
    # mesa
  ];


  installPhase = ''
    mkdir -p $out/bin
    cp -R * $out/
    chmod +x $out/konata
    ln -s $out/konata $out/bin/konata
  '';

  meta = with lib; {
    description = "Konata pipeline visualization tool";
    homepage = "https://github.com/shioyadan/Konata";
    platforms = platforms.linux;
  };
}
