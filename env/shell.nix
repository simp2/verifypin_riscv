/*
 * File: naxriscv.nix
 * Project: env
 * Created Date: Monday November 14th 2022
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 14th November 2022 3:45:58 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2022 INRIA
 */

let
    pkgs = import (builtins.fetchGit {
        # Descriptive name to make the store path easier to identify                
        name = "pinned_nix_packages";                                                 
        url = "https://github.com/nixos/nixpkgs/";                       
        ref = "nixos-22.05";                     
        rev = "ce6aa13369b667ac2542593170993504932eb836";                                           
    }) {};

    riscv-toolchain = (import ./riscv-medany-multilib-toolchain.nix);
    # konata = (import ./konata.nix);
in

with import <nixpkgs> { 
#     crossSystem = {
#         config = "riscv32-unknown-linux-gnu";
#   };
};

# Make a new "derivation" that represents our shell
stdenv.mkDerivation {
    name = "naxriscv-build";

    RISCV = toString riscv-toolchain;
    RISCV_PATH = toString riscv-toolchain;
    APP_DIR = toString ../application/build;
    CORE_DIR = toString ../Comet/build;

    # The packages in the `buildInputs` list will be added to the PATH in our shell
    nativeBuildInputs = with pkgs.python310Packages; [
        pkgs.cmake
        # spike
        # elfio
        # verilator
        # sbt
        pkgs.zlib
        pkgs.zstd
        # SDL2
        # dtc
        # boost
        # konata
        pyelftools
        termcolor
        tqdm
        pkgs.isl
        pkgs.libmpc
        riscv-toolchain
    ];

    commands = ''   
    SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
    '';

    shellHook = ''
        eval "$commands"
        export PATH=$PATH:$RISCV/bin 
    '';

}
